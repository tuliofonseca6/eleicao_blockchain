﻿using Models.Models;
using PersistenciaBD.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersistenciaBD.Services
{
    public class RegistroVotoPersistencia : IRegistroVotoPersistencia
    {
        public void cadastrarVinculoEleitoral(VinculoEleicao vinculo)
        {
            using (BD_Eleicao_BlockchainEntities entities = new BD_Eleicao_BlockchainEntities())
            {
                Eleicao eleicao = new Eleicao();
                eleicao.DataHora = DateTime.Now;
                eleicao.Votou = false;
                eleicao.Vinculo_Endereco = vinculo.Vinculo_Endereco;
                eleicao.Vinculo_Usuario = vinculo.Vinculo_Usuario;
                entities.Eleicao.Add(eleicao);
                entities.SaveChanges();
            }
        }

        public List<Eleicao> buscarEleitores()
        {
            List<Eleicao> eleicaos = new List<Eleicao>();
            using (BD_Eleicao_BlockchainEntities entities = new BD_Eleicao_BlockchainEntities())
            {
                eleicaos = entities.Eleicao.ToList();
            }
            return eleicaos;
        }
        public Eleicao buscaDadosPorUsuario(string usuarioCriptografado)
        {
            Eleicao eleicaoRetorno = null;

            using (BD_Eleicao_BlockchainEntities entities = new BD_Eleicao_BlockchainEntities())
            {
                eleicaoRetorno = entities.Eleicao.Where(eleicao => eleicao.Vinculo_Usuario == usuarioCriptografado).FirstOrDefault();
            }

            return eleicaoRetorno;
        }

        public bool registrarVoto(UsuarioVotacao usuarioVotacao)
        {
            bool votacaoFeita = false;

            using (BD_Eleicao_BlockchainEntities entities = new BD_Eleicao_BlockchainEntities())
            {
                Eleicao eleicaoRetorno = entities.Eleicao.Where(eleicao => eleicao.Vinculo_Usuario == usuarioVotacao.Vinculo_Usuario).FirstOrDefault();
                if (eleicaoRetorno == null)
                {
                    throw new Exception("Dados do eleitor não encontrados");
                }
                eleicaoRetorno.DataHora = usuarioVotacao.dataRandom;
                eleicaoRetorno.Votou = true;
                entities.SaveChanges();

                votacaoFeita = true;
            }

            return votacaoFeita;
        }
    }
}
