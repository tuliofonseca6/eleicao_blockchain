﻿using Models.Models;
using PersistenciaBD.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersistenciaBD.Services
{
    public class LoginPersistencia : ILoginPersistencia
    {
        public Usuario buscarLoginByCpf(string cpf)
        {
            Usuario usuario;
            using (BD_Eleicao_BlockchainEntities entities = new BD_Eleicao_BlockchainEntities())
            {
                usuario = entities.Usuario.Where(u => u.Cpf == cpf && u.TipoUsuario == 1).FirstOrDefault();
            }
            return usuario;
        }

        public Usuario buscarLoginByCpfETipo(string cpf)
        {
            Usuario usuario;
            using (BD_Eleicao_BlockchainEntities entities = new BD_Eleicao_BlockchainEntities())
            {
                usuario = entities.Usuario.Where(u => u.Cpf == cpf && u.TipoUsuario == 2).FirstOrDefault();
            }
            return usuario;
        }

        public Usuario cadastrarUsuario(LoginModel loginModel)
        {
            Usuario usuario = new Usuario();
            using (BD_Eleicao_BlockchainEntities entities = new BD_Eleicao_BlockchainEntities())
            {
                usuario.Nome = loginModel.NomeUsuario;
                usuario.Cpf = loginModel.UserCPF;
                usuario.Senha = loginModel.Password;
                usuario.TipoUsuario = 1;

                entities.Usuario.Add(usuario);
                entities.SaveChanges();
            }
            return usuario;
        }
    }
}
