﻿using Models.Models;
using PersistenciaBD.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersistenciaBD.Services
{
    public class ProcessoEleitoralPersistencia : IProcessoEleitoralPersistencia
    {
        public ProcessoEleitoral buscaProcessoAtivoParaDataAtual()
        {
            ProcessoEleitoral processoEleitoral = new ProcessoEleitoral();
            DateTime dateTimeNow = DateTime.Now;
            using (BD_Eleicao_BlockchainEntities entities = new BD_Eleicao_BlockchainEntities())
            {
                processoEleitoral = entities.ProcessoEleitoral.Where(pe => pe.DataInicio < dateTimeNow && pe.DataFim > dateTimeNow).FirstOrDefault();
            }
            return processoEleitoral;
        }

        public ProcessoEleitoral buscaProcessoFinalizadoParaDataAtual()
        {
            ProcessoEleitoral processoEleitoral = new ProcessoEleitoral();
            DateTime dateTimeNow = DateTime.Now;
            using (BD_Eleicao_BlockchainEntities entities = new BD_Eleicao_BlockchainEntities())
            {
                List<ProcessoEleitoral> processoEleitorais = entities.ProcessoEleitoral.ToList();
                if (processoEleitorais.Count > 0)
                {
                    List<ProcessoEleitoral> processoEleitorais2 = entities.ProcessoEleitoral.Where(pe => pe.DataInicio < dateTimeNow && pe.DataFim < dateTimeNow).ToList();
                    if (processoEleitorais.Count == processoEleitorais2.Count)
                    {
                        processoEleitoral.Ativo = true;
                    }
                    else
                    {
                        processoEleitoral.Ativo = false;
                    }
                 }
                else
                {
                    processoEleitoral.Ativo = false;
                    return processoEleitoral;
                }
            }
            return processoEleitoral;
        }

        public ProcessoEleitoral cadastrarNovoProcesso(ProcessoAtivoDTO processoAtivoDTO, Usuario usuario)
        {
            ProcessoEleitoral processoEleitoral = new ProcessoEleitoral();
            DateTime dateTimeInicio = DateTime.Parse(processoAtivoDTO.dataInicio);
            DateTime dateTimeFim = DateTime.Parse(processoAtivoDTO.dataFim);
            using (BD_Eleicao_BlockchainEntities entities = new BD_Eleicao_BlockchainEntities())
            {
                processoEleitoral.codUsuarioPK = usuario.CodUsuario;
                processoEleitoral.DataInicio = dateTimeInicio;
                processoEleitoral.DataFim = dateTimeFim;
                processoEleitoral.Ativo = true;
                processoEleitoral.nomeProcesso = processoAtivoDTO.nomeProcesso;
                entities.ProcessoEleitoral.Add(processoEleitoral);
                entities.SaveChanges();
            }


            return processoEleitoral;

        }

    }
}
