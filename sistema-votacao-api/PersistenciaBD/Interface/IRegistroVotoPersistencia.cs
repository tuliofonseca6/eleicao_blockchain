﻿using Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersistenciaBD.Interface
{
    public interface IRegistroVotoPersistencia
    {
        void cadastrarVinculoEleitoral(VinculoEleicao vinculo);
        bool registrarVoto(UsuarioVotacao usuarioVotacao);
        Eleicao buscaDadosPorUsuario(string usuarioCriptografado);
        List<Eleicao> buscarEleitores();
    }
}
