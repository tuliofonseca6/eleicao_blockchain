﻿using Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersistenciaBD.Interface
{
    public interface IProcessoEleitoralPersistencia
    {
        ProcessoEleitoral buscaProcessoAtivoParaDataAtual();

        ProcessoEleitoral cadastrarNovoProcesso(ProcessoAtivoDTO processoAtivoDTO, Usuario usuario);

        ProcessoEleitoral buscaProcessoFinalizadoParaDataAtual();
    }
}
