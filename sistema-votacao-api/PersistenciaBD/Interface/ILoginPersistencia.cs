﻿using Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersistenciaBD.Interface
{
    public interface ILoginPersistencia
    {
        Usuario buscarLoginByCpf(string cpf);
        Usuario cadastrarUsuario(LoginModel loginModel);

        Usuario buscarLoginByCpfETipo(string cpf);
    }
}
