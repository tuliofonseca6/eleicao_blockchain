﻿using Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RegrasNegocio.Interfaces
{
    public interface ILoginNegocio
    {
        ValidacaoLogin buscarLogin(LoginModel loginModel);
        ValidacaoLogin buscarLoginAdm(LoginModel loginModel);
        LoginModel cadastrarUsuario(LoginModel loginModel);
    }
}
