﻿using Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RegrasNegocio.Interfaces
{
    public interface IProcessoEleitoralNegocio
    {
        ProcessoAtivoModel buscarProcessoAtivo(LoginModel loginModel);

        ProcessoAtivoModel cadastrarNovoProcesso(ProcessoAtivoDTO loginModel);

    } 
}
