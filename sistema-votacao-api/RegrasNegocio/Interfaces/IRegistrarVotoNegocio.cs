﻿using Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RegrasNegocio.Interfaces
{
    public interface IRegistrarVotoNegocio
    {
        void cadastrarVinculoEleitoral(LoginModel loginModel);
        bool votar(LoginModel loginModel);
        DadosUsuario permissaoVotar(LoginModel loginModel);

        ApuracaoVotos apuracaoVotos(LoginModel loginModel);

    }
}
