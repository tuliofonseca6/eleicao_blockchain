﻿using Models.Models;
using PersistenciaBD;
using PersistenciaBD.Interface;
using RegrasNegocio.Interfaces;
using System;
using System.Collections.Generic;

namespace RegrasNegocio.Services
{
    public class RegistrarVotoNegocio : IRegistrarVotoNegocio
    {

        ILoginNegocio loginNegocio;
        ILoginPersistencia loginPersistencia;
        IRegistroVotoPersistencia registroVotoPersistencia;
        IProcessoEleitoralPersistencia processoEleitoralPersistencia;
        public RegistrarVotoNegocio(ILoginNegocio loginNegocio, IRegistroVotoPersistencia registroVotoPersistencia, IProcessoEleitoralPersistencia processoEleitoralPersistencia, ILoginPersistencia loginPersistencia)
        {
            this.loginNegocio = loginNegocio;
            this.registroVotoPersistencia = registroVotoPersistencia;
            this.processoEleitoralPersistencia = processoEleitoralPersistencia;
            this.loginPersistencia = loginPersistencia;
        }

        public void cadastrarVinculoEleitoral(LoginModel loginModel)
        {
            
            ValidacaoLogin loginValidado = loginNegocio.buscarLogin(loginModel);
            if (loginValidado != null && loginValidado.Is_Valido)
            {
                string vinculoUsuario = Hash.Codificar(loginModel.UserCPF);

                string vinculoEndereco = Hash.Codificar("0x73E31D09716CeDe0465727260e1E3E0C3C1C2f41");

                VinculoEleicao vinculo = new VinculoEleicao();
                vinculo.Vinculo_Endereco = vinculoEndereco;
                vinculo.Vinculo_Usuario = vinculoUsuario;
                registroVotoPersistencia.cadastrarVinculoEleitoral(vinculo);
            }
            else
            {
                throw new Exception("O usuário não existe");
            }
        }
        public bool votar(LoginModel loginModel)
        {
            ValidacaoLogin loginValidado = loginNegocio.buscarLogin(loginModel);
            if (loginValidado != null && loginValidado.Is_Valido)
            {
                Random random = new Random();

                int anoAtual = DateTime.Now.Year;
                int mesAtual = DateTime.Now.Month;
                int diaAtual = DateTime.Now.Day;
                int horaRandom = random.Next(23);
                int minutoRandom = random.Next(59);
                int segundosRandom = random.Next(59);
                DateTime dataRamdom = new DateTime(anoAtual, mesAtual, diaAtual, horaRandom, minutoRandom, segundosRandom);

                string vinculoUsuario = Hash.Codificar(loginModel.UserCPF);

                UsuarioVotacao usuarioVotacao = new UsuarioVotacao();
                usuarioVotacao.dataRandom = dataRamdom;
                usuarioVotacao.Vinculo_Usuario = vinculoUsuario;

                return registroVotoPersistencia.registrarVoto(usuarioVotacao);

            }
            else
            {
                throw new Exception("O usuário não existe");
            }
        }

        public DadosUsuario permissaoVotar(LoginModel loginModel)
        {
            ValidacaoLogin loginValidado = loginNegocio.buscarLogin(loginModel);
            if (loginValidado != null && loginValidado.Is_Valido)
            {
                DadosUsuario dadosUsuario = new DadosUsuario();

                string usuarioCriptografado = Hash.Codificar(loginModel.UserCPF);

                Eleicao eleicao = registroVotoPersistencia.buscaDadosPorUsuario(usuarioCriptografado);
                if (eleicao == null)
                {
                    throw new Exception("Dados do eleitor não encontrados");
                }

                ProcessoEleitoral processoEleitoral = processoEleitoralPersistencia.buscaProcessoAtivoParaDataAtual();

                if (processoEleitoral == null)
                {
                    dadosUsuario.periodoEleitoral = false;
                }
                else
                {
                    dadosUsuario.periodoEleitoral = true;
                }

                dadosUsuario.UserName = loginModel.UserCPF;
                dadosUsuario.enderecoCarteira = Hash.Decodificar(eleicao.Vinculo_Endereco);
                dadosUsuario.votou = eleicao.Votou;


                return dadosUsuario;
            }
            else
            {
                throw new Exception("O usuário não existe");
            }

        }

        public ApuracaoVotos apuracaoVotos(LoginModel loginModel)
        {
            ApuracaoVotos apuracaoVotos = new ApuracaoVotos();

            ProcessoEleitoral processoEleitoral = processoEleitoralPersistencia.buscaProcessoFinalizadoParaDataAtual();

            if (processoEleitoral != null)
            {
                if (processoEleitoral.Ativo == true)
                {
                    apuracaoVotos.is_Processo_Finalizado = true;
                    apuracaoVotos.eleitores = buscarEleitores();

                }
                else
                {
                    apuracaoVotos.is_Processo_Finalizado = false;
                    apuracaoVotos.eleitores = new List<Eleitor>();
                }
            }
            else
            {
                throw new Exception("Ocorreu um erro !");
            }

            return apuracaoVotos;
        }

        private List<Eleitor> buscarEleitores()
        {
            List<Eleitor> eleitores = new List<Eleitor>();

            List<Eleicao> lista = registroVotoPersistencia.buscarEleitores();
            if (lista != null && lista.Count > 0)
            {
                foreach (Eleicao eleicao in lista)
                {
                    string cpfUsuario = Hash.Decodificar(eleicao.Vinculo_Usuario);
                    Usuario usuario = loginPersistencia.buscarLoginByCpf(cpfUsuario);
                    Eleitor eleitor = new Eleitor();
                    eleitor.nomeEleitor = usuario.Nome;
                    eleitor.votou = eleicao.Votou;
                    eleitores.Add(eleitor);
                }
            }
            return eleitores;
        }
    }
}
