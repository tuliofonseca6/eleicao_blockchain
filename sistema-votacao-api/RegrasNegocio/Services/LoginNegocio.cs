﻿using Models.Models;
using PersistenciaBD;
using PersistenciaBD.Interface;
using RegrasNegocio.Interfaces;
using System;


namespace RegrasNegocio.Services
{
    public class LoginNegocio : ILoginNegocio
    {
        ILoginPersistencia loginPersistencia;
        IRegistroVotoPersistencia registroVotoPersistencia;

        public LoginNegocio(ILoginPersistencia loginPersistencia, IRegistroVotoPersistencia registroVotoPersistencia)
        {
            this.loginPersistencia = loginPersistencia;
            this.registroVotoPersistencia = registroVotoPersistencia;


        }
        public ValidacaoLogin buscarLogin(LoginModel loginModel)
        {
            ValidacaoLogin retorno = new ValidacaoLogin();
            retorno.UserName = loginModel.UserCPF;
            retorno.Is_Valido = false;
            if (loginModel == null || loginModel.UserCPF == null || loginModel.Password == null)
            {
                throw new Exception("O campo esta vazio");
            }
            Usuario loginBD = loginPersistencia.buscarLoginByCpf(loginModel.UserCPF);

            if (loginBD == null)
            {
                throw new Exception("O usuário não existe");
            }

            bool validaSenha = Hash.VerificarHash(loginModel.Password, loginBD.Senha);
            retorno.Is_Valido = validaSenha;

            return retorno;
        }

        public ValidacaoLogin buscarLoginAdm(LoginModel loginModel)
        {

            ValidacaoLogin retorno = new ValidacaoLogin();
            retorno.UserName = loginModel.UserCPF;
            retorno.Is_Valido = false;
            if (loginModel == null || loginModel.UserCPF == null || loginModel.Password == null)
            {
                throw new Exception("O campo esta vazio");
            }
            Usuario loginBD = loginPersistencia.buscarLoginByCpfETipo(loginModel.UserCPF);

            if (loginBD == null)
            {
                throw new Exception("O usuario não existe");
            }

            bool validaSenha = Hash.VerificarHash(loginModel.Password, loginBD.Senha);
            retorno.Is_Valido = validaSenha;

            return retorno;
        }
        public LoginModel cadastrarUsuario(LoginModel loginModel)
        {
            if (loginModel.Password == null || loginModel.Password.Equals("") || loginModel.UserCPF == null || loginModel.UserCPF.Equals(""))
            {
                throw new Exception("Ocorreu um erro ao cadastrar o usuario, há dados vazios ou não preenchido!");
            }
            loginModel.Password = Hash.Codificar(loginModel.Password);
            Usuario loginBD = loginPersistencia.cadastrarUsuario(loginModel);
            if (loginBD != null)
            {
                cadastrarVinculoEleitoral(loginModel);
                LoginModel loginUsuario = converterModels(loginBD);
                return loginUsuario;
            }
            else
            {
                throw new Exception("Ocorreu um erro ao cadastrar o usuario");
            }
           
        }

        private LoginModel converterModels(Usuario loginBD)
        {
            LoginModel loginModel = new LoginModel();
            loginModel.NomeUsuario = loginBD.Nome;
            loginModel.UserCPF = loginBD.Cpf;
            loginModel.Password = loginBD.Senha;
            return loginModel;
        }

        private void cadastrarVinculoEleitoral(LoginModel loginModel)
        {
            try
            {
                string vinculoUsuario = Hash.Codificar(loginModel.UserCPF);

                string vinculoEndereco = Hash.Codificar("0x73E31D09716CeDe0465727260e1E3E0C3C1C2f41");

                VinculoEleicao vinculo = new VinculoEleicao();
                vinculo.Vinculo_Endereco = vinculoEndereco;
                vinculo.Vinculo_Usuario = vinculoUsuario;
                registroVotoPersistencia.cadastrarVinculoEleitoral(vinculo);
            }
            catch (Exception ex)
            {
                throw new Exception("Erro ao cadastrar o vinculo eleitoral do usuario, erro: " + ex.Message);
            }
        }

    }
}
