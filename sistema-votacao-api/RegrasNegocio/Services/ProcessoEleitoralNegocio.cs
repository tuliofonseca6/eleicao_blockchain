﻿using Models.Models;
using PersistenciaBD;
using PersistenciaBD.Interface;
using RegrasNegocio.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RegrasNegocio.Services
{
    public class ProcessoEleitoralNegocio : IProcessoEleitoralNegocio
    {
        IProcessoEleitoralPersistencia processoEleitoralPersistencia;
        ILoginPersistencia loginPersistencia;

        public ProcessoEleitoralNegocio(IProcessoEleitoralPersistencia processoEleitoralPersistencia, ILoginPersistencia loginPersistencia)
        {
            this.processoEleitoralPersistencia = processoEleitoralPersistencia;
            this.loginPersistencia = loginPersistencia;
        }
        public ProcessoAtivoModel buscarProcessoAtivo(LoginModel loginModel)
        {
            ProcessoAtivoModel processoAtivoModel = new ProcessoAtivoModel();
            ProcessoEleitoral processoEleitoral = processoEleitoralPersistencia.buscaProcessoAtivoParaDataAtual();
            if (processoEleitoral == null)
            {
                throw new Exception("Não há processo eleitoral atualmente");
            }

            Usuario usuario = loginPersistencia.buscarLoginByCpfETipo(loginModel.UserCPF);

            if (usuario == null)
            {
                throw new Exception("Não foi possivel achar o usuario quem criou o processo ativo atual");
            }

            processoAtivoModel.DataFim = processoEleitoral.DataFim.ToString();
            processoAtivoModel.DataInicio = processoEleitoral.DataInicio.ToString();
            processoAtivoModel.NomePessoa = usuario.Nome;
            processoAtivoModel.NomeProcesso = processoEleitoral.nomeProcesso;
            return processoAtivoModel;
        }

        public ProcessoAtivoModel cadastrarNovoProcesso(ProcessoAtivoDTO processoAtivoDTO)
        {
            ProcessoAtivoModel processoAtivoModel = new ProcessoAtivoModel();
            ProcessoEleitoral processoEleitoral = processoEleitoralPersistencia.buscaProcessoAtivoParaDataAtual();
            if (processoEleitoral != null)
            {
                throw new Exception("Já existe um processo eleitoral ativo em vigor.");
            }

            Usuario usuario = loginPersistencia.buscarLoginByCpfETipo(processoAtivoDTO.loginModel.UserCPF);

            if (usuario == null)
            {
                throw new Exception("Não foi possivel achar o usuario para ser registrado no processo que será criado");
            }
            ProcessoEleitoral novoProcessoEleitoral = processoEleitoralPersistencia.cadastrarNovoProcesso(processoAtivoDTO, usuario);

            processoAtivoModel.DataFim = novoProcessoEleitoral.DataFim.ToString();
            processoAtivoModel.DataInicio = novoProcessoEleitoral.DataInicio.ToString();
            processoAtivoModel.NomePessoa = usuario.Nome;
            processoAtivoModel.NomeProcesso = novoProcessoEleitoral.nomeProcesso;

            return processoAtivoModel;

        }
    }
}
