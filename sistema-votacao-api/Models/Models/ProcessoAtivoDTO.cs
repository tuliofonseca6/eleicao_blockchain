﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Models
{
    public class ProcessoAtivoDTO
    {
        public LoginModel loginModel { get; set; }
        public String nomeProcesso { get; set; }
        public String dataInicio { get; set; }
        public String dataFim { get; set; }
    }
}
