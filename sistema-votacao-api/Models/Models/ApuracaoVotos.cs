﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Models
{
    public class ApuracaoVotos
    {
        public Boolean is_Processo_Finalizado { get; set; }
        public List<Eleitor> eleitores { get; set; }
    }
}
