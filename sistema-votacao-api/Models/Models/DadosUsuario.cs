﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Models
{
    public class DadosUsuario
    {
        public string UserName { get; set; }
        public string enderecoCarteira { get; set; }
        public bool votou { get; set; }
        public bool periodoEleitoral { get; set; }
    }
}
