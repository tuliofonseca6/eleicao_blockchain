﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Models
{
    public class LoginModel
    {
        public string NomeUsuario { get; set; }
        public string UserCPF { get; set; }
        public string Password { get; set; }
    }
}
