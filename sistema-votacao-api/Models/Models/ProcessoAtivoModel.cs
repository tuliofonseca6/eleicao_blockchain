﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Models
{
    public class ProcessoAtivoModel
    {
        public String DataInicio { get; set; }
        public String DataFim { get; set; }
        public String NomePessoa { get; set; }
        public String NomeProcesso { get; set; }
    }
}
