﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Models
{
    public class VinculoEleicao
    {
        public string Vinculo_Endereco { get; set; }
        public string Vinculo_Usuario { get; set; }
    }
}
