﻿using Modelos.Models;
using Negocio.Interfaces;
using System;
using System.Security.Cryptography;
using System.Text;


namespace Negocio.Services
{
    public class LoginNegocio : ILoginNegocio
    {
        /*LoginPersistencia loginPersistencia;
        public LoginNegocio(LoginPersistencia loginPersistencia)
        {
            this.loginPersistencia = loginPersistencia;
        }*/
        public LoginModel buscarLogin(LoginModel loginModel)
        {
            /*if (loginModel == null || loginModel.UserName == null || loginModel.Password == null)
            {
                throw new Exception("O campo esta vazio");
            }
            Usuario loginBD = loginPersistencia.buscarLoginByCpf(int.Parse(loginModel.UserName));
            if (loginBD == null)
            {
                loginModel.Password = ComputeSHA256Hash(loginModel.Password);
                loginBD = loginPersistencia.cadastrarUsuario(loginModel);
            }
            LoginModel loginUsuario = converterModels(loginBD);
            return loginUsuario;*/
            return null;
        }
        private string ComputeSHA256Hash(string text)
        {
            using (var sha256 = new SHA256Managed())
            {
                return BitConverter.ToString(sha256.ComputeHash(Encoding.UTF8.GetBytes(text))).Replace("-", "");
            }
        }

        /*private LoginModel converterModels(Usuario loginBD)
        {
            LoginModel loginModel = new LoginModel();
            loginModel.UserName = loginBD.Cpf.ToString();
            loginModel.Password = loginBD.Senha;
            return loginModel;
        }*/
    }
}
