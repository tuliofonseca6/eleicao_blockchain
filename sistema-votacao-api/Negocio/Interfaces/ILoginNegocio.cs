﻿using Modelos.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Negocio.Interfaces
{
    public interface ILoginNegocio
    {
        LoginModel buscarLogin(LoginModel loginModel);
    }
}
