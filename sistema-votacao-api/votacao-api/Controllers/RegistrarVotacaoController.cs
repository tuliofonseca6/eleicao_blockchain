﻿using Models.Models;
using RegrasNegocio.Interfaces;
using RegrasNegocio.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace votacao_api.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class RegistrarVotacaoController : ApiController
    {
        private IRegistrarVotoNegocio registrarVotoNegocio;

        public RegistrarVotacaoController(IRegistrarVotoNegocio registrarVotoNegocio)
        {
            this.registrarVotoNegocio = registrarVotoNegocio;
        }

        [Route("api/buscaPermissaoVotar")]
        [HttpPost]
        public DadosUsuario buscaPermissaoVotar(LoginModel loginModel)
        {
            DadosUsuario dadosUsuario = registrarVotoNegocio.permissaoVotar(loginModel);
            if (dadosUsuario != null)
            {
                return dadosUsuario;
            }
            return null;
        }

        [Route("api/cadastrarVinculoEleitoral")]
        [HttpPost]
        public void cadastrarVinculoEleitoral(LoginModel loginModel)
        {
            this.registrarVotoNegocio.cadastrarVinculoEleitoral(loginModel);
        }

        [Route("api/votar")]
        [HttpPost]
        public bool votar(LoginModel loginModel)
        {
            bool votou = registrarVotoNegocio.votar(loginModel);
            if (votou != null)
            {
                return votou;
            }
            return false;
        }

        [Route("api/apuracaoVotos")]
        [HttpPost]
        public ApuracaoVotos apuracaoVotos(LoginModel loginModel)
        {

            ApuracaoVotos apuracaoVotos = registrarVotoNegocio.apuracaoVotos(loginModel);
            if (apuracaoVotos == null)
            {
                throw new Exception("Ocorreu um erro ao recuperar a apuração dos votos.");
            }
            return apuracaoVotos;

        }

    }
}
