﻿using Models.Models;
using RegrasNegocio.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace votacao_api.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class ProcessoEleitoralController : ApiController
    {
        private IProcessoEleitoralNegocio processoEleitoralNegocio;

        public ProcessoEleitoralController(IProcessoEleitoralNegocio processoEleitoralNegocio)
        {
            this.processoEleitoralNegocio = processoEleitoralNegocio;
        }

        [Route("api/processoAtivo")]
        [HttpPost]
        public ProcessoAtivoModel processoAtivo(LoginModel loginModel)
        {

            ProcessoAtivoModel processoAtivo = processoEleitoralNegocio.buscarProcessoAtivo(loginModel);
            if (processoAtivo != null)
            {
                return processoAtivo;
            }
            return null;

        }

        [Route("api/cadastrarProcesso")]
        [HttpPost]
        public ProcessoAtivoModel cadastrarProcesso(ProcessoAtivoDTO processoAtivoDTO)
        {

            ProcessoAtivoModel processoCadastrado = processoEleitoralNegocio.cadastrarNovoProcesso(processoAtivoDTO);
            if (processoCadastrado == null)
            {
                throw new Exception("Ocorreu um erro ao cadastrar o novo processo.");
            }
            return processoCadastrado;

        }

    }
}
