﻿using Models.Models;
using RegrasNegocio.Interfaces;
using System.Web.Http;
using System.Web.Http.Cors;

namespace votacao_api.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class LoginController : ApiController
    {

        private ILoginNegocio loginNegocio;

        public LoginController(ILoginNegocio loginNegocio)
        {
            this.loginNegocio = loginNegocio;
        }

        [Route("api/login")]
        [HttpPost]
        public ValidacaoLogin login(LoginModel loginModel)
        {

            ValidacaoLogin login = loginNegocio.buscarLogin(loginModel);
            if (login != null)
            {
                return login;
            }
            return null;

        }

        [Route("api/loginAdm")]
        [HttpPost]
        public ValidacaoLogin loginAdm(LoginModel loginModel)
        {

            ValidacaoLogin login = loginNegocio.buscarLoginAdm(loginModel);
            if (login != null)
            {
                return login;
            }
            return null;

        }

        [Route("api/cadastroEleitor")]
        [HttpPost]
        public LoginModel cadastroEleitor(LoginModel loginModel)
        {

            LoginModel login = loginNegocio.cadastrarUsuario(loginModel);
            if (login != null)
            {
                return login;
            }
            return null;

        }


    }
}
