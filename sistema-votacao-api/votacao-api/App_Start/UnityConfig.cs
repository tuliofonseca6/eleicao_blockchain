using PersistenciaBD.Interface;
using PersistenciaBD.Services;
using RegrasNegocio.Interfaces;
using RegrasNegocio.Services;
using System.Web.Http;
using Unity;
using Unity.WebApi;

namespace votacao_api
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
            var container = new UnityContainer();

            // register all your components with the container here
            // it is NOT necessary to register your controllers

            // e.g. container.RegisterType<ITestService, TestService>();

            container.RegisterType<ILoginNegocio, LoginNegocio>();
            container.RegisterType<ILoginPersistencia, LoginPersistencia>();

            container.RegisterType<IRegistrarVotoNegocio, RegistrarVotoNegocio>();
            container.RegisterType<IRegistroVotoPersistencia, RegistroVotoPersistencia>();

            container.RegisterType<IProcessoEleitoralNegocio, ProcessoEleitoralNegocio>();
            container.RegisterType<IProcessoEleitoralPersistencia, ProcessoEleitoralPersistencia>();

            GlobalConfiguration.Configuration.DependencyResolver = new UnityDependencyResolver(container);
        }
    }
}