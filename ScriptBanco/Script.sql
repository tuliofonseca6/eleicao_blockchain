USE [master]
GO
/****** Object:  Database [BD_Eleicao_Blockchain]    Script Date: 29/11/2020 20:39:08 ******/
CREATE DATABASE [BD_Eleicao_Blockchain]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'BD_Eleicao_Blockchain', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.SQLEXPRESS\MSSQL\DATA\BD_Eleicao_Blockchain.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'BD_Eleicao_Blockchain_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.SQLEXPRESS\MSSQL\DATA\BD_Eleicao_Blockchain_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [BD_Eleicao_Blockchain] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [BD_Eleicao_Blockchain].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [BD_Eleicao_Blockchain] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [BD_Eleicao_Blockchain] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [BD_Eleicao_Blockchain] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [BD_Eleicao_Blockchain] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [BD_Eleicao_Blockchain] SET ARITHABORT OFF 
GO
ALTER DATABASE [BD_Eleicao_Blockchain] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [BD_Eleicao_Blockchain] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [BD_Eleicao_Blockchain] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [BD_Eleicao_Blockchain] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [BD_Eleicao_Blockchain] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [BD_Eleicao_Blockchain] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [BD_Eleicao_Blockchain] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [BD_Eleicao_Blockchain] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [BD_Eleicao_Blockchain] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [BD_Eleicao_Blockchain] SET  DISABLE_BROKER 
GO
ALTER DATABASE [BD_Eleicao_Blockchain] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [BD_Eleicao_Blockchain] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [BD_Eleicao_Blockchain] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [BD_Eleicao_Blockchain] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [BD_Eleicao_Blockchain] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [BD_Eleicao_Blockchain] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [BD_Eleicao_Blockchain] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [BD_Eleicao_Blockchain] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [BD_Eleicao_Blockchain] SET  MULTI_USER 
GO
ALTER DATABASE [BD_Eleicao_Blockchain] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [BD_Eleicao_Blockchain] SET DB_CHAINING OFF 
GO
ALTER DATABASE [BD_Eleicao_Blockchain] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [BD_Eleicao_Blockchain] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [BD_Eleicao_Blockchain] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [BD_Eleicao_Blockchain] SET QUERY_STORE = OFF
GO
USE [BD_Eleicao_Blockchain]
GO
/****** Object:  Table [dbo].[Eleicao]    Script Date: 29/11/2020 20:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Eleicao](
	[Vinculo_Usuario] [nvarchar](max) NOT NULL,
	[Vinculo_Endereco] [nvarchar](max) NOT NULL,
	[Votou] [bit] NOT NULL,
	[DataHora] [datetime] NOT NULL,
	[CodEleicao] [bigint] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_Eleicao] PRIMARY KEY CLUSTERED 
(
	[CodEleicao] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ProcessoEleitoral]    Script Date: 29/11/2020 20:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProcessoEleitoral](
	[CodProcessoEleitoral] [bigint] IDENTITY(1,1) NOT NULL,
	[DataInicio] [datetime] NOT NULL,
	[DataFim] [datetime] NULL,
	[Ativo] [bit] NOT NULL,
	[codUsuarioPK] [bigint] NOT NULL,
	[nomeProcesso] [varchar](250) NULL,
 CONSTRAINT [PK_ProcessoEleitoral] PRIMARY KEY CLUSTERED 
(
	[CodProcessoEleitoral] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Usuario]    Script Date: 29/11/2020 20:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Usuario](
	[CodUsuario] [bigint] IDENTITY(1,1) NOT NULL,
	[Nome] [nvarchar](max) NOT NULL,
	[Cpf] [varchar](max) NOT NULL,
	[Senha] [nvarchar](max) NOT NULL,
	[TipoUsuario] [int] NULL,
 CONSTRAINT [PK_Usuario] PRIMARY KEY CLUSTERED 
(
	[CodUsuario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Eleicao] ON 

INSERT [dbo].[Eleicao] ([Vinculo_Usuario], [Vinculo_Endereco], [Votou], [DataHora], [CodEleicao]) VALUES (N'y38NOOUnyahAvynsk5xDcg==', N'QAxU6pzbx36MwewqgCMbXGLR6+wj7CknMPYFC4vJ/onxwtcsjNMAz7oiSeE9mhT1', 1, CAST(N'2020-10-27T05:01:40.000' AS DateTime), 2)
INSERT [dbo].[Eleicao] ([Vinculo_Usuario], [Vinculo_Endereco], [Votou], [DataHora], [CodEleicao]) VALUES (N'A99bQkMCT+9J00USGUTxfg==', N'QAxU6pzbx36MwewqgCMbXGLR6+wj7CknMPYFC4vJ/onxwtcsjNMAz7oiSeE9mhT1', 1, CAST(N'2020-10-28T13:27:04.000' AS DateTime), 3)
INSERT [dbo].[Eleicao] ([Vinculo_Usuario], [Vinculo_Endereco], [Votou], [DataHora], [CodEleicao]) VALUES (N'Hky2Q8kcLe4OCehSQ0vAiw==', N'QAxU6pzbx36MwewqgCMbXGLR6+wj7CknMPYFC4vJ/onxwtcsjNMAz7oiSeE9mhT1', 1, CAST(N'2020-11-18T13:14:38.000' AS DateTime), 4)
INSERT [dbo].[Eleicao] ([Vinculo_Usuario], [Vinculo_Endereco], [Votou], [DataHora], [CodEleicao]) VALUES (N'YXkkw8V18E7BYZKen5ggCQ==', N'QAxU6pzbx36MwewqgCMbXGLR6+wj7CknMPYFC4vJ/onxwtcsjNMAz7oiSeE9mhT1', 1, CAST(N'2020-10-29T20:26:45.000' AS DateTime), 5)
INSERT [dbo].[Eleicao] ([Vinculo_Usuario], [Vinculo_Endereco], [Votou], [DataHora], [CodEleicao]) VALUES (N'zbqE94pnunVk9LW3kDcHpA==', N'QAxU6pzbx36MwewqgCMbXGLR6+wj7CknMPYFC4vJ/onxwtcsjNMAz7oiSeE9mhT1', 1, CAST(N'2020-11-18T14:29:02.000' AS DateTime), 10003)
INSERT [dbo].[Eleicao] ([Vinculo_Usuario], [Vinculo_Endereco], [Votou], [DataHora], [CodEleicao]) VALUES (N'WdK3aBwG9f0USbkmLel9Yw==', N'QAxU6pzbx36MwewqgCMbXGLR6+wj7CknMPYFC4vJ/onxwtcsjNMAz7oiSeE9mhT1', 0, CAST(N'2020-11-18T10:57:57.977' AS DateTime), 10004)
INSERT [dbo].[Eleicao] ([Vinculo_Usuario], [Vinculo_Endereco], [Votou], [DataHora], [CodEleicao]) VALUES (N'qKo1IOg6ED/DxUnCL+kVnA==', N'QAxU6pzbx36MwewqgCMbXGLR6+wj7CknMPYFC4vJ/onxwtcsjNMAz7oiSeE9mhT1', 1, CAST(N'2020-11-18T02:58:54.000' AS DateTime), 10005)
SET IDENTITY_INSERT [dbo].[Eleicao] OFF
SET IDENTITY_INSERT [dbo].[ProcessoEleitoral] ON 

INSERT [dbo].[ProcessoEleitoral] ([CodProcessoEleitoral], [DataInicio], [DataFim], [Ativo], [codUsuarioPK], [nomeProcesso]) VALUES (1, CAST(N'2020-09-26T11:15:11.000' AS DateTime), CAST(N'2020-09-29T11:15:11.000' AS DateTime), 0, 10002, N'Processo Teste')
INSERT [dbo].[ProcessoEleitoral] ([CodProcessoEleitoral], [DataInicio], [DataFim], [Ativo], [codUsuarioPK], [nomeProcesso]) VALUES (2, CAST(N'2020-09-09T11:15:11.000' AS DateTime), CAST(N'2020-09-30T11:15:11.000' AS DateTime), 1, 10002, N'Processo A')
INSERT [dbo].[ProcessoEleitoral] ([CodProcessoEleitoral], [DataInicio], [DataFim], [Ativo], [codUsuarioPK], [nomeProcesso]) VALUES (3, CAST(N'2019-10-28T00:00:00.000' AS DateTime), CAST(N'2019-10-28T00:00:00.000' AS DateTime), 1, 10002, N'Processo Estrela')
INSERT [dbo].[ProcessoEleitoral] ([CodProcessoEleitoral], [DataInicio], [DataFim], [Ativo], [codUsuarioPK], [nomeProcesso]) VALUES (4, CAST(N'2019-10-28T00:00:00.000' AS DateTime), CAST(N'2019-10-28T00:00:00.000' AS DateTime), 1, 10002, N'Processo Estrela 5')
INSERT [dbo].[ProcessoEleitoral] ([CodProcessoEleitoral], [DataInicio], [DataFim], [Ativo], [codUsuarioPK], [nomeProcesso]) VALUES (10002, CAST(N'2020-10-28T00:00:00.000' AS DateTime), CAST(N'2021-10-28T00:00:00.000' AS DateTime), 1, 10002, N'Processo Estrela 6')
SET IDENTITY_INSERT [dbo].[ProcessoEleitoral] OFF
SET IDENTITY_INSERT [dbo].[Usuario] ON 

INSERT [dbo].[Usuario] ([CodUsuario], [Nome], [Cpf], [Senha], [TipoUsuario]) VALUES (10002, N'TulioAdm', N'12345678910', N'wJxSBLcGw4SR5msH1Sbuxw==', 2)
INSERT [dbo].[Usuario] ([CodUsuario], [Nome], [Cpf], [Senha], [TipoUsuario]) VALUES (10003, N'Tulio', N'12345678911', N'wJxSBLcGw4SR5msH1Sbuxw==', 1)
INSERT [dbo].[Usuario] ([CodUsuario], [Nome], [Cpf], [Senha], [TipoUsuario]) VALUES (20003, N'Carlos', N'12345678912', N'wJxSBLcGw4SR5msH1Sbuxw==', 1)
INSERT [dbo].[Usuario] ([CodUsuario], [Nome], [Cpf], [Senha], [TipoUsuario]) VALUES (20004, N'Tomas', N'12345678913', N'wJxSBLcGw4SR5msH1Sbuxw==', 1)
INSERT [dbo].[Usuario] ([CodUsuario], [Nome], [Cpf], [Senha], [TipoUsuario]) VALUES (20005, N'Pessoa', N'12345678914', N'wJxSBLcGw4SR5msH1Sbuxw==', 1)
INSERT [dbo].[Usuario] ([CodUsuario], [Nome], [Cpf], [Senha], [TipoUsuario]) VALUES (30005, N'Tulio Teste 1', N'12345678915', N'wJxSBLcGw4SR5msH1Sbuxw==', 1)
INSERT [dbo].[Usuario] ([CodUsuario], [Nome], [Cpf], [Senha], [TipoUsuario]) VALUES (30006, N'Tulio Teste 6', N'12345678917', N'wJxSBLcGw4SR5msH1Sbuxw==', 1)
INSERT [dbo].[Usuario] ([CodUsuario], [Nome], [Cpf], [Senha], [TipoUsuario]) VALUES (30007, N'Tulio Teste 7', N'12345678916', N'wJxSBLcGw4SR5msH1Sbuxw==', 1)
SET IDENTITY_INSERT [dbo].[Usuario] OFF
/****** Object:  Index [idx_Cpf]    Script Date: 29/11/2020 20:39:08 ******/
CREATE UNIQUE NONCLUSTERED INDEX [idx_Cpf] ON [dbo].[Usuario]
(
	[CodUsuario] ASC
)
INCLUDE([Cpf]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ProcessoEleitoral]  WITH CHECK ADD  CONSTRAINT [FK_ProcessoEleitoral_Usuario] FOREIGN KEY([codUsuarioPK])
REFERENCES [dbo].[Usuario] ([CodUsuario])
GO
ALTER TABLE [dbo].[ProcessoEleitoral] CHECK CONSTRAINT [FK_ProcessoEleitoral_Usuario]
GO
USE [master]
GO
ALTER DATABASE [BD_Eleicao_Blockchain] SET  READ_WRITE 
GO
