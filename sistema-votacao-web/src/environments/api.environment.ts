export const apiPaths = {
    login: 'login',
    loginAdm:'loginAdm',
    buscaPermissaoVotar : 'buscaPermissaoVotar',
    votar : 'votar',
    cadastroEleitor : 'cadastroEleitor',
    processoAtivo : 'processoAtivo',
    cadastrarProcesso : 'cadastrarProcesso',
    apuracaoVotos : 'apuracaoVotos'
}