import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import {MatCardModule} from '@angular/material/card';
import { TransacaoComponent } from './ExemploTransacao/exemplo-transacao/transacao/transacao.component';
import { LoginComponent } from './Login/login/login/login.component';
import { HttpClientModule } from '@angular/common/http';
import { EthcontractService } from './ExemploTransacao/exemplo-transacao/Service/ethcontract.service';
import { MatDialogModule } from '@angular/material/dialog';
import {MatButtonModule} from '@angular/material/button';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LoginAdmComponent } from './login-adm/login-adm.component';
import {MatInputModule} from '@angular/material/input';
import { VotacaoEleitralComponent } from './Votacao/votacao-eleitral/votacao-eleitral.component';

@NgModule({
  declarations: [
    AppComponent,
    TransacaoComponent,
    LoginComponent,
    VotacaoEleitralComponent,
    LoginAdmComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    MatDialogModule,
    MatButtonModule,
    BrowserAnimationsModule,
    MatCardModule,
    MatInputModule,
  ],
  providers: [EthcontractService],
  bootstrap: [AppComponent],
})
export class AppModule { }
