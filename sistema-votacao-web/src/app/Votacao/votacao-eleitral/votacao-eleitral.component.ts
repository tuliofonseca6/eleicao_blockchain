import { Component, OnInit, Inject, Output, EventEmitter } from '@angular/core';
import { ServiceTransacaoService } from '../../ServiceTransacao/service-transacao.service';
import { Location } from '@angular/common';
import { Politico } from '../../Modelos/Politicos';
import { EthcontractService } from '../../ExemploTransacao/exemplo-transacao/Service/ethcontract.service';


const ENDERECOS : Politico = {
  'dados':[  {'nome':'Fulano1','endereco':'0xE7E90FeA1B29De89FAB4d8d123AFBCb09DED7edE'}, 
  {'nome':'Fulano2','endereco':'0xE399BF438C0c2C171BdEb9b5E0eA4ACE1C6b50dC'},
  {'nome':'Fulano3','endereco':'0x65DF53d310d11Cda49A7DEC9FA1828Ad6dd3A0d2'},
  {'nome':'Fulano4','endereco':'0x9839d8a6a40e8375F8CeC973928EeEEb0e14A372'},
  {'nome':'Fulano5','endereco':'0x6a703a57219406C418BaE3d9bD091cAbf70c0169'}]
};



@Component({
  selector: 'app-votacao-eleitral',
  templateUrl: './votacao-eleitral.component.html',
  styleUrls: ['./votacao-eleitral.component.css']
})
export class VotacaoEleitralComponent implements OnInit {

  constructor(private location: Location, private serviceTransacaoService : ServiceTransacaoService,  private ethcontractService : EthcontractService) { }
  escolhido : string;
  modoConfirmacao : boolean = false;
  estado : number = 1;
  carregando : boolean = false;
  public tela = 1;
  @Output() telaLogin = new EventEmitter();



  async ngOnInit(): Promise<void> {
    var user =  window.sessionStorage.getItem('usuario');
    var pass = window.sessionStorage.getItem('password');
    await this.serviceTransacaoService.jaVotou(user,pass).then((usuario : any)=>{
        if(usuario.votou == true){
          this.estado = 3; 
        }
    });

  }

  votar(nome : string){
    this.escolhido = nome;
    this.modoConfirmacao = true;
    this.estado = 2;
  }

  confirmarVotacao(){
    this.carregando = true;
    var nome = this.escolhido; 
    var politico = ENDERECOS.dados.find( p => p.nome ===  nome);
    var enderecoOrigem = window.sessionStorage.getItem('enderecoCarteira');
    this.ethcontractService.transferEther(enderecoOrigem,politico.endereco,"1").then((status : any) => {
        if (status.status === true){
          var user =  window.sessionStorage.getItem('usuario');
          var pass = window.sessionStorage.getItem('password');
          this.serviceTransacaoService.RegistrarVoto(user, pass).then((retorno : any) => {
            if (retorno === true){
              this.tela = 4;
              this.telaLogin.emit(this.tela);
            }
          });
        }
    }).finally(() => {
      this.carregando = false;
    });
  }
  load() {
    location.reload()
  }
  
  voltarTela(){
    this.estado = 1;
  }

  realizaLogout(){
    window.sessionStorage.removeItem('usuario');
    window.sessionStorage.removeItem('enderecoCarteira');
    window.sessionStorage.removeItem('password');
    this.serviceTransacaoService.setIsLogado(false);
    this.serviceTransacaoService.setIsCarregando(false);
    this.telaLogin.emit(this.tela);
  }
}

