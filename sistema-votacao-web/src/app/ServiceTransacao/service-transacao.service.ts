import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { ErroMessage } from '../Modelos/ErrorMessage';
import { DadosUsuario } from '../Modelos/DadosUsuario';
import { ProcessoAtivoModel } from '../Modelos/ProcessoAtivoModel';

class LoginModel{
  UserCPF: String;
  Password: String;
  NomeUsuario: String;
}

class ProcessoAtivoDTO {
  loginModel : LoginModel;
  nomeProcesso: String;
  dataInicio : String;
  dataFim : String;
}

class Eleitor{
  nomeEleitor : string;
  votou : boolean;
}

class ApuracaoVotos {
  is_Processo_Finalizado : boolean;
  eleitores : Eleitor[];
}

@Injectable({
  providedIn: 'root'
})
export class ServiceTransacaoService {

  constructor(private http: HttpClient) { }

  private is_logado : boolean = false;

  private eleitorCadastrado : boolean = false;
  
  private is_carregando : boolean = false;

  private periodoEleitoralAberto : boolean = false;

  private dashboardAdm : boolean = false;

  setPeriodoEleitoralAberto (periodo : boolean){
    this.periodoEleitoralAberto = periodo;
  }

  setEleitorCadastrado (estado : boolean){
    this.eleitorCadastrado = estado;
  }

  getEleitorCadastrado(){
    return this.eleitorCadastrado;
  }

  setDashboardAdm (permissao : boolean){
    this.dashboardAdm = permissao;
  }

  getDashboardAdm(){
    return this.dashboardAdm;
  }

  getPeriodoEleitoralAberto (){
    return this.periodoEleitoralAberto;
  }

  setIsLogado (estado : boolean) {
    this.is_logado = estado;
  }

  getIsLogado (){
    return this.is_logado;
  }

  setIsCarregando (estado : boolean){
    this.is_carregando = estado;
  }

  getIsCarregando (){
    return this.is_carregando;
  }

  async RegistrarVoto (UserCPF, Password){
    var loginForm = new LoginModel();
    loginForm.UserCPF = UserCPF;
    loginForm.Password = Password;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type' : 'application/json',
        'application' : 'x-www-form-urlencoded',
        'Access-Control-Allow-Origin': 'http://localhost:4200',
        'Access-Control-Allow-Credentials': 'true'
      })
    };
    return await this.http.post( environment.host + environment.apiPaths.votar,  loginForm, httpOptions)
    .toPromise().then((status : boolean) => {
      return status;
    }).catch((error : ErroMessage) => {
      alert(error.error.Message + ' ' + error.error.ExceptionMessage + '.');
    });
  }

  async buscarProcessosAtivos(UserCPF, Password) {
    var loginForm = new LoginModel();
    loginForm.UserCPF = UserCPF;
    loginForm.Password = Password;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type' : 'application/json',
        'application' : 'x-www-form-urlencoded',
        'Access-Control-Allow-Origin': 'http://localhost:4200',
        'Access-Control-Allow-Credentials': 'true'
      })
    };
    return await this.http.post( environment.host + environment.apiPaths.processoAtivo,  loginForm, httpOptions)
    .toPromise().then((processoAtivoModel : ProcessoAtivoModel) => {
      return processoAtivoModel;
    }).catch((error : ErroMessage) => {
      alert(error.error.Message + ' ' + error.error.ExceptionMessage + '.');
    });
  }

  async buscarApuracaoVotos(UserCPF, Password) {
    var loginForm = new LoginModel();
    loginForm.NomeUsuario = "";
    loginForm.UserCPF = UserCPF;
    loginForm.Password = Password;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type' : 'application/json',
        'application' : 'x-www-form-urlencoded',
        'Access-Control-Allow-Origin': 'http://localhost:4200',
        'Access-Control-Allow-Credentials': 'true'
      })
    };
    return await this.http.post( environment.host + environment.apiPaths.apuracaoVotos,  loginForm, httpOptions)
    .toPromise().then((apuracaoVotos : ApuracaoVotos) => {
      if (apuracaoVotos.is_Processo_Finalizado === true){
        alert("Requisição realizada com sucesso, atualizando a tela ...");
      }
      return apuracaoVotos;
    }).catch((error : ErroMessage) => {
      alert(error.error.Message + ' ' + error.error.ExceptionMessage + '.');
    });
  }

  async cadastrarProcesso(processoAtivoDTO : ProcessoAtivoDTO) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type' : 'application/json',
        'application' : 'x-www-form-urlencoded',
        'Access-Control-Allow-Origin': 'http://localhost:4200',
        'Access-Control-Allow-Credentials': 'true'
      })
    };
    return await this.http.post( environment.host + environment.apiPaths.cadastrarProcesso,  processoAtivoDTO, httpOptions)
    .toPromise().then((processoAtivoModel : ProcessoAtivoModel) => {
      alert("Processo cadastrado com sucesso");
    }).catch((error : ErroMessage) => {
      alert(error.error.Message + ' ' + error.error.ExceptionMessage + '.');
    });
  }

  async jaVotou(UserCPF, Password) {
    var loginForm = new LoginModel();
    loginForm.UserCPF = UserCPF;
    loginForm.Password = Password;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type' : 'application/json',
        'application' : 'x-www-form-urlencoded',
        'Access-Control-Allow-Origin': 'http://localhost:4200',
        'Access-Control-Allow-Credentials': 'true'
      })
    };
    return await this.http.post( environment.host + environment.apiPaths.buscaPermissaoVotar,  loginForm, httpOptions)
    .toPromise().then((usuario : DadosUsuario) => {
      return usuario;
    }).catch((error : ErroMessage) => {
      alert(error.error.Message + ' ' + error.error.ExceptionMessage + '.');
    });
  }

}
