/*import { Injectable } from '@angular/core';
import Web3 from 'web3';
import TruffleContract from 'truffle-contract';
import { Usuario } from './Usuario';
declare let require: any;
declare let window: any;

//let tokenAbi = require('../../../build/contracts/Payment.json');
let tokenAbi = require('C:/Users/TulioFonseca/Desktop/votacao_Blockchain/build/contracts/Payment.json');



@Injectable({
  providedIn: 'root'
})
export class EthcontractService {
  private _account: string = null;
  private _web3: any;
  private _tokenContract: any;
  private _tokenContractAddress: string = "0xbc84f3bf7dd607a37f9e5848a6333e6c188d926c";

  private web3Provider: any;
  private contracts: {};
  constructor() {
    if (typeof window.web3 !== 'undefined') {
      this.web3Provider = window.web3.currentProvider;
    } else {
      this.web3Provider = new Web3.providers.HttpProvider('http://localhost:7545');
    }
    window.web3 = new Web3(this.web3Provider);
    
  }
  getAccountInfo(): Promise<Usuario> {
    return new Promise((resolve, reject) => {
      window.web3.eth.getCoinbase(function (err, account) {
        if (err === null) {
          window.web3.eth.getBalance(account, function (err, balance) {
            if (err === null) {
              debugger;
              var usuario = new Usuario(account, window.web3.fromWei(balance, "ether"));
              return usuario;
              //return resolve({ fromAccount: account, balance: window.web3.fromWei(balance, "ether") });
            } else {
              return reject("error!");
            }
          });
        }
      });
    });
  }

  transferEther(
    _transferFrom,
    _transferTo,
    _amount,
    _remarks
  ) {
    let that = this;
    return new Promise((resolve, reject) => {
      let paymentContract = TruffleContract(tokenAbi);
      paymentContract.setProvider(that.web3Provider);
      paymentContract.deployed().then(function (instance) {
        return instance.transferFund(
          _transferTo,
          {
            from: _transferFrom,
            value: window.web3.toWei(_amount, "ether")
          });
      }).then(function (status) {
        if (status) {
          return resolve({ status: true });
        }
      }).catch(function (error) {
        console.log(error);
        return reject("Error in transferEther service call");
      });
    });
  }

}*/

import { Injectable } from '@angular/core';
import Web3 from 'web3';
import * as TruffleContract from 'truffle-contract';

declare let require: any;
declare let window: any;

let tokenAbi = require('../../../build/contracts/Payment.json');

@Injectable({
  providedIn: 'root'
})

export class EthcontractService {
  private web3Provider: any;
  private contracts: {};


  constructor() {
    //debugger;
    /*if (typeof window.web3 !== 'undefined') {
      this.web3Provider = window.web3.currentProvider;
    } else {
      this.web3Provider = new Web3.providers.HttpProvider('http://localhost:7545');
    }*/
    this.web3Provider = new Web3.providers.HttpProvider('http://localhost:7545');
    let provider = new Web3.providers.HttpProvider('http://localhost:7545');
    window.web3 = new Web3(provider);
    //}
    //window.web3 = new Web3(this.web3Provider);
  }

  getAccountInfo() {

    return new Promise((resolve, reject) => {
      window.web3.eth.getCoinbase(function(err, account) {
        if(err === null) {
          window.web3.eth.getBalance(account, function(err, balance) {
            if(err === null) {
              var teste = Number(window.web3.utils.fromWei(balance, "ether"));
              return resolve({fromAccount: account, balance:Number(window.web3.utils.fromWei(balance, "ether"))});
            } else {
              return reject({fromAccount: "error", balance:0});
            }
          });
        }
      });
    });
  }

  transferEther(
    _transferFrom,
    _transferTo,
    _amount,
    _remarks
  ) {
    let that = this;

    return new Promise((resolve, reject) => {
      let paymentContract = TruffleContract(tokenAbi);
      paymentContract.setProvider(that.web3Provider);

      paymentContract.deployed().then(function(instance) {
          return instance.transferFund(
            _transferTo,
            {
              from:_transferFrom,
              value:window.web3.utils.toWei(_amount, "ether")
            });
        }).then(function(status) {
          if(status) {
            return resolve({status:true});
          }
        }).catch(function(error){
          console.log(error);

          return reject("Error in transferEther service call");
        });
    });
  }
}

