import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { LoginService } from '../Login/login/Service/login.service';
import { ServiceTransacaoService } from '../ServiceTransacao/service-transacao.service';

@Component({
  selector: 'app-login-adm',
  templateUrl: './login-adm.component.html',
  styleUrls: ['./login-adm.component.css']
})
export class LoginAdmComponent implements OnInit {

  formModel = {
    UserCPF: '',
    Password: ''
  }
  constructor(private loginService : LoginService, private serviceTransacaoService: ServiceTransacaoService) { }
  public tela = 1;

  @Output() telaLogin = new EventEmitter();

  ngOnInit(): void {
  }

  login() {
    
  }

  mudancaTela(valor){
    console.log ("valor:" + valor)
    if (valor == 1){
      debugger;
      this.loginService.loginAdm(this.formModel).then(() => {
        
        if (this.serviceTransacaoService.getDashboardAdm() === true){
          this.tela = 10
          this.telaLogin.emit(this.tela);
          return this.tela;
        }else{
          this.telaLogin.emit(this.tela);
          return this.tela;
        }
      });
    }else{
      this.telaLogin.emit(this.tela);
      return this.tela;
    }
  }

}
