import { Injectable } from '@angular/core';
import { environment } from '../../../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ValidacaoLogin } from '../../../Modelos/ValidacaoLogin';
import { ErroMessage } from '../../../Modelos/ErrorMessage';
import { ServiceTransacaoService } from '../../../ServiceTransacao/service-transacao.service';
import { DadosUsuario } from '../../../Modelos/DadosUsuario';

class LoginModel {
  UserCPF: String;
  Password: String;
  NomeUsuario: String;
}

class CadastroEleitorModel {
  NomeUsuario: String;
  UserCPF: String;
  Password: String;
}

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http: HttpClient, private serviceTransacaoService: ServiceTransacaoService) { }



  login(formData){
    var loginForm = new LoginModel();
    loginForm.NomeUsuario = "";
    loginForm.UserCPF = formData.UserCPF;
    loginForm.Password = formData.Password;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'application': 'x-www-form-urlencoded',
        'Access-Control-Allow-Origin': 'http://localhost:4200',
        'Access-Control-Allow-Credentials': 'true'
      })
    };
    return this.http.post(environment.host + environment.apiPaths.login, loginForm, httpOptions).toPromise()
      .then(async (login: ValidacaoLogin) => {
        if (login.Is_Valido && login.Is_Valido != undefined && login.Is_Valido == true) {
          alert('Você autenticou no sistema.');
          debugger;
          window.sessionStorage.setItem('usuario', loginForm.UserCPF.toString());
          window.sessionStorage.setItem('password', loginForm.Password.toString());
          this.serviceTransacaoService.setIsLogado(true);
          await this.serviceTransacaoService.jaVotou(loginForm.UserCPF, loginForm.Password).then((usuario: DadosUsuario) => {
            console.log(usuario);
            window.sessionStorage.setItem('enderecoCarteira', usuario.enderecoCarteira.toString());
            this.serviceTransacaoService.setIsCarregando(usuario.votou);
            this.serviceTransacaoService.setPeriodoEleitoralAberto(usuario.periodoEleitoral);
          }).catch((error: ErroMessage) => {
            alert(error.error.Message + ' ' + error.error.ExceptionMessage + '.');
          });
        } else {
          alert('Login ou senha incorreto.');
        }
      }).catch((error: ErroMessage) => {
        alert(error.error.Message + ' ' + error.error.ExceptionMessage + '.');
      });
  }

  loginAdm(formData){
    var loginForm = new LoginModel();
    loginForm.UserCPF = formData.UserCPF;
    loginForm.Password = formData.Password;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'application': 'x-www-form-urlencoded',
        'Access-Control-Allow-Origin': 'http://localhost:4200',
        'Access-Control-Allow-Credentials': 'true'
      })
    };
    return this.http.post(environment.host + environment.apiPaths.loginAdm, loginForm, httpOptions).toPromise()
      .then(async (login: ValidacaoLogin) => {
        if (login.Is_Valido && login.Is_Valido != undefined && login.Is_Valido == true) {
          alert('Você autenticou no sistema.');
          window.sessionStorage.setItem('usuario', loginForm.UserCPF.toString());
          window.sessionStorage.setItem('password', loginForm.Password.toString());
          this.serviceTransacaoService.setIsLogado(true);
          this.serviceTransacaoService.setDashboardAdm(true);
        } else {
          alert('Login ou senha incorreto.');
        }
      }).catch((error: ErroMessage) => {
        alert(error.error.Message + ' ' + error.error.ExceptionMessage + '.');
      });
  }

  cadastrarEleitor(formData){
    var loginForm = new CadastroEleitorModel();
    loginForm.UserCPF = formData.UserCPF;
    loginForm.Password = formData.Password;
    loginForm.NomeUsuario = formData.NomeUsuario;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'application': 'x-www-form-urlencoded',
        'Access-Control-Allow-Origin': 'http://localhost:4200',
        'Access-Control-Allow-Credentials': 'true'
      })
    };
    return this.http.post(environment.host + environment.apiPaths.cadastroEleitor, loginForm, httpOptions).toPromise()
      .then(async (eleitor: CadastroEleitorModel) => {
        if (eleitor.NomeUsuario && eleitor.NomeUsuario != undefined && eleitor.UserCPF && eleitor.UserCPF != undefined) {
          alert('Eleitor Cadastrado com sucesso');
          this.serviceTransacaoService.setEleitorCadastrado(true);
        } else {
          alert('Ocorreu um erro ao cadastrar um eleitor.');
        }
      }).catch((error: ErroMessage) => {
        alert(error.error.Message + ' ' + error.error.ExceptionMessage + '.');
      });
  }

  loginBack(username, password) {
    var loginForm = new LoginModel();
    loginForm.UserCPF = username;
    loginForm.Password = password;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'application': 'x-www-form-urlencoded',
        'Access-Control-Allow-Origin': 'http://localhost:4200',
        'Access-Control-Allow-Credentials': 'true'
      })
    };
    return this.http.post(environment.host + environment.apiPaths.login, loginForm, httpOptions)
      .toPromise().then((login: ValidacaoLogin) => {
        if (login.Is_Valido && login.Is_Valido != undefined && login.Is_Valido == true) {
          return true;
        } else {
          return false;
        }
      }).catch((error: ErroMessage) => {
        alert(error.error.Message + ' ' + error.error.ExceptionMessage + '.');
      });
  }

}
