import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { NgForm } from '@angular/forms';
import { LoginService } from '../Service/login.service';
import { ServiceTransacaoService } from '../../../ServiceTransacao/service-transacao.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  formModel = {
    UserCPF: '',
    Password: ''
  }
  constructor(private loginService : LoginService, private serviceTransacaoService: ServiceTransacaoService) { }
  public tela = 1;

  @Output() telaLogin = new EventEmitter();

  ngOnInit(): void {
  }

  login() {
    
  }

  mudancaTela(valor){
    console.log ("valor:" + valor)
    if (valor == 1){
      this.loginService.login(this.formModel).then(() => {
        if (this.serviceTransacaoService.getIsLogado() == false){
          this.telaLogin.emit(this.tela);
          return this.tela;
        }
        if (this.serviceTransacaoService.getIsCarregando() == false){
          if (this.serviceTransacaoService.getPeriodoEleitoralAberto() == false){
            this.tela = 9
            this.telaLogin.emit(this.tela);
            return this.tela;
          }
          this.tela = 3
          this.telaLogin.emit(this.tela);
          return this.tela;
        }else{
          this.tela = 4
          this.telaLogin.emit(this.tela);
          return this.tela;
        }
      });
    }else{
      this.telaLogin.emit(this.tela);
      return this.tela;
    }
  }

}
