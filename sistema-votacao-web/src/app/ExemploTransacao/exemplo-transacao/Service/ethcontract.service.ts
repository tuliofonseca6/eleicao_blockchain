import { Injectable } from '@angular/core';
import Web3 from 'web3';
import * as TruffleContract from 'truffle-contract';

declare let require: any;
declare let window: any;

let tokenAbi = require('../../../../../../build/contracts/Payment.json');

@Injectable({
  providedIn: 'root'
})

export class EthcontractService {
  private web3Provider: any;
  private contracts: {};


  constructor() {
    //debugger;
    /*if (typeof window.web3 !== 'undefined') {
      this.web3Provider = window.web3.currentProvider;
    } else {
      this.web3Provider = new Web3.providers.HttpProvider('http://localhost:7545');
    }*/
    this.web3Provider = new Web3.providers.HttpProvider('http://localhost:7545');
    let provider = new Web3.providers.HttpProvider('http://localhost:7545');
    window.web3 = new Web3(provider);
    //}
    //window.web3 = new Web3(this.web3Provider);
  }

  getAccountInfo() {

    return new Promise((resolve, reject) => {
      window.web3.eth.getCoinbase(function(err, account) {
        if(err === null) {
          window.web3.eth.getBalance(account, function(err, balance) {
            if(err === null) {
              var teste = Number(window.web3.utils.fromWei(balance, "ether"));
              return resolve({fromAccount: account, balance:Number(window.web3.utils.fromWei(balance, "ether"))});
            } else {
              return reject({fromAccount: "error", balance:0});
            }
          });
        }
      });
    });
  }

  getInfos(account){
    return new Promise ((resolve, reject) => {
      window.web3.eth.getBalance(account, function(err, balance) {
        if(err === null) {
          return resolve({fromAccount: account, balance:Number(window.web3.utils.fromWei(balance, "ether"))});
        } else {
          return reject({fromAccount: "error", balance:0});
        }
      });
    });
  }

getAccounts(){
  console.log(window.web3.eth.accounts.wallet);

  /*return new Promise ((resolve, reject) => {
    window.web3.eth.accounts.wallet(function(err, accounts) {
      if(err === null) {
        console.log(accounts);
        return resolve({enderecos: accounts});
      } else {
        return reject({enderecos: "error"});
      }
    });
  });*/
}

  

  transferEther(
    _transferFrom,
    _transferTo,
    _amount
  ) {
    let that = this;

    return new Promise((resolve, reject) => {
      let paymentContract = TruffleContract(tokenAbi);
      paymentContract.setProvider(that.web3Provider);

      paymentContract.deployed().then(function(instance) {
          return instance.transferFund(
            _transferTo,
            {
              from:_transferFrom,
              value:window.web3.utils.toWei(_amount, "ether")
            });
        }).then(function(status) {
          if(status) {
            return resolve({status:true});
          }
        }).catch(function(error){
          console.log(error);

          return reject("Error in transferEther service call");
        });
    });
  }
}

