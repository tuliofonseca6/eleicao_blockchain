import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { EthcontractService } from '../Service/ethcontract.service';


@Component({
  selector: 'app-transacao',
  templateUrl: './transacao.component.html',
  styleUrls: ['./transacao.component.css']
})
export class TransacaoComponent implements OnInit {
  title = 'your first DApp in Angular';
  accounts:any;
  transferFrom = '';
  balance ='0 ETH';
  transferTo='';
  amount=0;
  remarks='';
  erro : boolean = false;
  public tela = 1;
  @Output() telaLogin = new EventEmitter();

  ngOnInit() {
    this.initAndDisplayAccount();
  }

  constructor( private ethcontractService: EthcontractService ){
    this.initAndDisplayAccount();
  }

  initAndDisplayAccount = () => {
    let that = this;
    this.ethcontractService.getAccountInfo().then(function(acctInfo : any){
      this.erro = false;
      //that.transferFrom = acctInfo.fromAccount;
      //that.balance = acctInfo.balance;
    }).catch(function(error){
      console.log(error);
    });

  };

  async transferEther(event){
    try {
      let that = this;
      debugger;
      await this.ethcontractService.transferEther(
        this.transferFrom,
        this.transferTo,
        "1"
      ).then(function(){
        that.initAndDisplayAccount();
      }).catch(function(error){
        console.log(error);
        this.erro = true;
        that.initAndDisplayAccount();
      });
    } catch (error) {
      this.erro = true;
    }
  }

  reset(){
    this.erro = false;
  }

  voltar(){
    this.erro = false;
    this.telaLogin.emit(this.tela);
    return this.tela;
  }
}