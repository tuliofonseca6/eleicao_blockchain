import { Component, EventEmitter, Output } from '@angular/core';
import { EthcontractService } from './ExemploTransacao/exemplo-transacao/Service/ethcontract.service';
import { Politico, Dados } from './Modelos/Politicos';
import { DadosPoliticos } from './Modelos/DadosPoliticos';
import { ServiceTransacaoService } from './ServiceTransacao/service-transacao.service';
import { LoginService } from './Login/login/Service/login.service';
import { ProcessoAtivoModel } from './Modelos/ProcessoAtivoModel';

class LoginModel {
  UserCPF: String;
  Password: String;
  NomeUsuario: String;
}

class ProcessoAtivoDTO {
  loginModel : LoginModel;
  nomeProcesso: String;
  dataInicio : String;
  dataFim : String;
}

class Eleitor{
  nomeEleitor : string;
  votou : boolean;
}

class ApuracaoVotos {
  is_Processo_Finalizado : boolean;
  eleitores : Eleitor[];
}

const ENDERECOS : Politico = {
  'dados':[  {'nome':'Fulano1','endereco':'0xE7E90FeA1B29De89FAB4d8d123AFBCb09DED7edE'}, 
  {'nome':'Fulano2','endereco':'0xE399BF438C0c2C171BdEb9b5E0eA4ACE1C6b50dC'},
  {'nome':'Fulano3','endereco':'0x65DF53d310d11Cda49A7DEC9FA1828Ad6dd3A0d2'},
  {'nome':'Fulano4','endereco':'0x9839d8a6a40e8375F8CeC973928EeEEb0e14A372'},
  {'nome':'Fulano5','endereco':'0x6a703a57219406C418BaE3d9bD091cAbf70c0169'}]
};

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  public dadosPoliticos : any[] = [];
  public log : boolean = false;
  public imagePath = '/assets/eleicao.png';
  public imageLogin = '/assets/login.png';
  public imagejaVotou = '/assets/jaVotou.jpg';
  public imageAdm = '/assets/admLogin.png';
  public imageAguarde = '/assets/aguarde.jpg';
  public imageVoto = '/assets/voto.png';
  public imageCadastroUsuario = '/assets/cadastroUsuario.jpg';
  
  
  public tela = 1;
  formModel = {
    NomeUsuario: '',
    Password: '',
    UserCPF: ''
  }
  formModelCadastroProcesso = {
    NomeProcesso: '',
    DataInicio: '',
    DataFim: '',
  }

  public nomeProcessoAtual : string = '';
  public nomePessoa : string = '';
  public dataInicio : string = '';
  public dataFim : string = '';
  public existeProcesso : boolean = false;
  public apuracaoVotos : boolean = false;
  public eleitores : Eleitor[];

  constructor( private ethcontractService: EthcontractService, private serviceTransacaoService : ServiceTransacaoService, private loginService : LoginService ){
    this.buscarDados(ethcontractService);
  }
  logado(){
    if (this.serviceTransacaoService.getIsLogado() == true){
      return true;
    }
    return false;
  }

  async buscarProcessoAtivo(){
    var user =  window.sessionStorage.getItem('usuario');
    var pass = window.sessionStorage.getItem('password');
    await this.serviceTransacaoService.buscarProcessosAtivos(user, pass).then((processoAtivoModel : ProcessoAtivoModel) => {
      this.nomePessoa = processoAtivoModel.NomePessoa;
      this.nomeProcessoAtual = processoAtivoModel.NomeProcesso;
      this.dataInicio = processoAtivoModel.DataInicio;
      this.dataFim = processoAtivoModel.DataFim;
      this.existeProcesso = true;
      return true;
    });

    return false;
  }

  possuiProcessoAtivo(){
    return this.existeProcesso;
  }

  async buscarApuracaoVotos(){
    var user =  window.sessionStorage.getItem('usuario');
    var pass = window.sessionStorage.getItem('password');
    await this.serviceTransacaoService.buscarApuracaoVotos(user, pass).then((apuracaoVotos : ApuracaoVotos)=>{
        this.apuracaoVotos = apuracaoVotos.is_Processo_Finalizado;
        this.eleitores = apuracaoVotos.eleitores;
    });
  }

  chamaTelaTesteInvalido(){
    this.tela = 20;
  }

  chamaTelaEleitor(){
    this.tela = 2;
  }

  sairDashboardAdm(){
    this.tela = 1;
  }

  async cadastrarProcesso(){
    var user =  window.sessionStorage.getItem('usuario');
    var pass = window.sessionStorage.getItem('password');
    var processoAtivoDTO = new ProcessoAtivoDTO();
    processoAtivoDTO.loginModel = new LoginModel();
    processoAtivoDTO.loginModel.NomeUsuario = "";
    processoAtivoDTO.loginModel.UserCPF = user;
    processoAtivoDTO.loginModel.Password = pass;
    processoAtivoDTO.dataInicio = this.formModelCadastroProcesso.DataInicio;
    processoAtivoDTO.dataFim = this.formModelCadastroProcesso.DataFim;
    processoAtivoDTO.nomeProcesso = this.formModelCadastroProcesso.NomeProcesso;
    await this.serviceTransacaoService.cadastrarProcesso(processoAtivoDTO).then(() => {

    });
  }

  cadastrarUsuario(){
    this.loginService.cadastrarEleitor(this.formModel).then(() => {
      if (this.serviceTransacaoService.getEleitorCadastrado() === true){
        this.serviceTransacaoService.setEleitorCadastrado(false);
        this.tela = 10;
      }else{
        this.serviceTransacaoService.setEleitorCadastrado(false);
        this.tela = 6
      }
    });
  }

  chamaTelaAdm(){
    this.tela = 5
  }

  atualizavalor(valor){
    this.tela = valor;
  }
  
  realizaLogout(){
    window.sessionStorage.removeItem('usuario');
    window.sessionStorage.removeItem('enderecoCarteira');
    window.sessionStorage.removeItem('password');
    this.serviceTransacaoService.setIsLogado(false);
    this.serviceTransacaoService.setIsCarregando(false);
    this.serviceTransacaoService.setDashboardAdm(false);
    this.serviceTransacaoService.setPeriodoEleitoralAberto(false);
    this.serviceTransacaoService.setEleitorCadastrado(false);
    this.tela = 1;
  }

  chamaTelaCadastroEleitor(){
    this.tela = 6;
  }

  chamaTelaDashboardAdm(){
    this.tela = 10;
  }


  chamaTelaProcessoEleitoral(){
    this.tela = 7;
  }
  
  chamaTelaApuracaoVotos(){
    this.tela = 8;
  }

  buscarDados(ethcontractService){
    let that = this;
    ENDERECOS.dados.forEach(async function (value: Dados) {
      await ethcontractService.getInfos(value.endereco).then(function(acctInfo : any){
        var dados = new DadosPoliticos();
        dados.nome = value.nome;
        if (dados.nome === 'Fulano1'){
          var votos =  acctInfo.balance - 101;
        } else if (dados.nome === 'Fulano2'){
          var votos =  acctInfo.balance - 102;
        }
        else if (dados.nome === 'Fulano3'){
          var votos =  acctInfo.balance - 105;
        }
        else if (dados.nome === 'Fulano4'){
          var votos =  acctInfo.balance - 105;
        }else{
          var votos =  acctInfo.balance - 103;
        }
        dados.votos = votos.toString();
        that.dadosPoliticos.push(dados);
      }).catch(function(error){
        console.log(error);
      });
    });
    //console.log(that.dadosPoliticos);
  }



  title = 'your first DApp in Angular';
  accounts:any;
  transferFrom = '0x0';
  balance ='0 ETH';
  transferTo='';
  amount=0;
  remarks='';



  initAndDisplayAccount = () => {
    let that = this;
    this.ethcontractService.getAccountInfo().then(function(acctInfo : any){
      that.transferFrom = acctInfo.fromAccount;
      that.balance = acctInfo.balance;
    }).catch(function(error){
      console.log(error);
    });

  };

  transferEther(event){
    let that = this;
console.log(this.transferTo);
    this.ethcontractService.transferEther(
      this.transferFrom,
      this.transferTo,
      this.amount,
    ).then(function(){
      that.initAndDisplayAccount();
    }).catch(function(error){
      console.log(error);
      that.initAndDisplayAccount();
    });
  }
}