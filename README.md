Para executar o projeto deve-se seguir os seguintes passos:
- Primeiramente dê um git clone no projeto.
- Em seguida crie uma blochchain privada em seu computador usando o Ganache e por fim execute a aplicação angular e dotnet.


Truffle Ganache (Criando e executando uma blochchain privada)
- Baixe e instale o Truffle Ganache em seu computador.
- Ao abrir o programa vá em "Contracts" e adicone o caminho do truffle-config.js que veio junto ao realizar o git clone.
- Clique em restartar a aplicação.
- Rode o comando "truffle migrate --compile-all --reset --network ganache" no terminal do vscode ou windows powershell
- Terminado.


Web
- Abra o VsCode.
- Execute o comando "npm install"
- Execute o comando "npm start" ou "ng serve" para iniciar a aplicação Web.
- Terminado.


API

- Abra o Microsoft Visual Studio.
- Execute o projeto.
- Terminado.

Script
- Execute o script da pasta ScriptBanco no SQL Server.
- Terminado.